# Order Service

Application available on port `8081` 

### Requirements
* Java 11
* RabbitMQ
* MongoDB
* Product Service (external service)
* Customer Service (external service)

### How to run application

IntelliJ IDEA

 `Build` > `Run...` > `OrderApplication`

Command-line

```shell
./gradlew bootRun
```

Run MongoDB and RabbitMQ

```shell
docker-compose up -d
```

### API Documentation

* generated by code
* available after the application run `/api/swagger-ui/index.html`