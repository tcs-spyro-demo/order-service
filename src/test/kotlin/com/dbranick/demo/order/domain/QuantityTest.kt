package com.dbranick.demo.order.domain

import assertk.assertThat
import assertk.assertions.isFailure
import assertk.assertions.isInstanceOf
import org.junit.jupiter.api.Test

internal class QuantityTest {

	@Test
	fun `should throw exception when value less than zero`() {
		//given
		val value = "-1".toBigDecimal()

		//when
		assertThat { Quantity(value) }
			//then
			.isFailure()
			.isInstanceOf(QuantityMustBeGreaterOrEqualsZero::class)
	}
}