package com.dbranick.demo.order.domain

import assertk.all
import assertk.assertThat
import assertk.assertions.*
import com.dbranick.demo.order.domain.event.OrderCreated
import com.dbranick.demo.order.domain.event.ProductLineCreated
import com.dbranick.demo.order.domain.event.ProductLineQuantityChanged
import com.dbranick.demo.order.helper.TestEventBus
import com.dbranick.demo.order.helper.productInvalid
import com.dbranick.demo.order.helper.productIsValid
import org.junit.jupiter.api.Test

internal class OrderTest {

	@Test
	fun `should add product to order`() {
		//given
		val eventBus = TestEventBus()
		val customerId = CustomerId("customer")
		val order = Order.new(eventBus, customerId)
		val product = ProductId("p1")
		val quantity = Quantity("2.3".toBigDecimal())

		//when
		order.addProduct(eventBus, product, quantity, productIsValid())

		//then
		assertThat(order.productLines).all {
			isNotEmpty()
			transform { it.findByProduct(product) }.all {
				isNotNull()
				prop("quantity") { it?.quantity }.isEqualTo(quantity)
				prop("product") { it?.product }.isEqualTo(product)
			}
		}

		assertThat(eventBus.eventBusCollector).all {
			hasSize(2)
			transform { it.map { event -> event::class } }
				.containsOnly(
					OrderCreated::class,
					ProductLineCreated::class
				)
		}
	}

	@Test
	fun `should change product quantity when product exist`() {
		//given
		val eventBus = TestEventBus()
		val customerId = CustomerId("customer")
		val order = Order.new(eventBus, customerId)
		val product = ProductId("p1")
		val quantity = Quantity("2.3".toBigDecimal())

		//when
		order.addProduct(eventBus, product, quantity, productIsValid())
		order.addProduct(eventBus, product, quantity, productIsValid())

		//then
		assertThat(order.productLines).all {
			isNotEmpty()
			transform { it.findByProduct(product) }.all {
				isNotNull()
				prop("quantity") { it?.quantity }.isEqualTo(quantity * 2)
				prop("product") { it?.product }.isEqualTo(product)
			}
		}

		assertThat(eventBus.eventBusCollector).all {
			hasSize(3)
			transform { it.map { event -> event::class } }
				.containsOnly(
					OrderCreated::class,
					ProductLineCreated::class,
					ProductLineQuantityChanged::class
				)
		}
	}

	@Test
	fun `should throw exception when try add product but product is invalid`() {
		//given
		val eventBus = TestEventBus()
		val customerId = CustomerId("customer")
		val order = Order.new(eventBus, customerId)
		val product = ProductId("p1")
		val quantity = Quantity("2.3".toBigDecimal())

		//when
		assertThat { order.addProduct(eventBus, product, quantity, productInvalid()) }

			//then
			.isFailure()
			.isInstanceOf(ProductInvalidException::class)

		assertThat(eventBus.eventBusCollector).hasSize(1)
	}

	@Test
	fun `should throw exception when try add product to order but order status is not NEW`() {
		//given
		val eventBus = TestEventBus()
		val customerId = CustomerId("customer")
		val order = Order.new(eventBus, customerId)
		val product = ProductId("p1")
		val quantity = Quantity("2.3".toBigDecimal())

		//when
		order.addProduct(eventBus, product, quantity, productIsValid())
		order.selectPaymentMethod(eventBus, PaymentMethodId("pm"))
		assertThat { order.addProduct(eventBus, product, quantity, productIsValid()) }

			//then
			.isFailure()
			.isInstanceOf(ProductModificationNotAvailableOnPresentStatusException::class)
		assertThat(eventBus.eventBusCollector).hasSize(2)
	}
}