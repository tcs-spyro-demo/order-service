package com.dbranick.demo.order.application.command

import assertk.all
import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNotNull
import assertk.assertions.prop
import com.dbranick.demo.order.domain.Order
import com.dbranick.demo.order.domain.OrderRepository
import com.dbranick.demo.order.domain.ProductValidationPolicy
import com.dbranick.demo.order.domain.event.EventBus
import io.mockk.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class OrderCommandServiceTest {

	private lateinit var orderCommandService: OrderCommandService
	private val orderRepository: OrderRepository = mockk()
	private val domainEventBus: EventBus = mockk()
	private val productValidationPolicy: ProductValidationPolicy = mockk()

	@BeforeEach
	fun setUp() {
		orderCommandService = OrderCommandService(
			orderRepository,
			domainEventBus,
			productValidationPolicy
		)
	}

	@Test
	fun `should create order`() {
		//given
		val command = object : CreateOrderCommand {
			override val customerId: String = "customer"
		}
		val captureSlot = CapturingSlot<Order>()
		every { domainEventBus.publish(any()) } just Runs
		every { orderRepository.save(capture(captureSlot)) } just Runs

		//when
		val result = orderCommandService.createOrder(command)

		//then
		assertThat(captureSlot.captured).all {
			isNotNull()
			prop("orderId") { it.orderId.value }.isEqualTo(result)
			prop("customer") { it.customer.value }.isEqualTo(command.customerId)
		}

		verify(exactly = 1) { domainEventBus.publish(any()) }
	}
}