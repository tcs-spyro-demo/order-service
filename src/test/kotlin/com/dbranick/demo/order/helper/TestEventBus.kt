package com.dbranick.demo.order.helper

import com.dbranick.demo.order.domain.event.DomainEvent
import com.dbranick.demo.order.domain.event.EventBus

internal class TestEventBus : EventBus {

	val eventBusCollector = mutableListOf<DomainEvent>()

	override fun publish(event: DomainEvent) {
		eventBusCollector.add(event)
	}
}