package com.dbranick.demo.order.helper

import com.dbranick.demo.order.domain.ProductId
import com.dbranick.demo.order.domain.ProductValidationPolicy

internal fun productIsValid() = TestProductValidationPolicy(true)
internal fun productInvalid() = TestProductValidationPolicy(false)

internal class TestProductValidationPolicy(
	private val isValid: Boolean
) : ProductValidationPolicy {
	override fun isValid(productId: ProductId): Boolean = isValid
}