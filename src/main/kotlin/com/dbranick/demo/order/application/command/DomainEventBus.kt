package com.dbranick.demo.order.application.command

import com.dbranick.demo.order.domain.event.*
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Component


@Component
internal class DomainEventBus(
	private val applicationEventPublisher: ApplicationEventPublisher,
	private val rabbitTemplate: RabbitTemplate
) : EventBus {

	override fun publish(event: DomainEvent) {
		applicationEventPublisher.publishEvent(event)
		publishExternal(event)
	}

	private fun publishExternal(event: DomainEvent) {
		when (event) {
			is OrderCreated,
			is OrderPaid,
			is OrderConfirmed,
			is OrderPrepared,
			is OrderDelivered -> rabbitTemplate.convertAndSend("orderQueue", event)
			else -> { }
		}
	}
}