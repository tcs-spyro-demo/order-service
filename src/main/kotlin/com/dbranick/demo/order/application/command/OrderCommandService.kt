package com.dbranick.demo.order.application.command

import com.dbranick.demo.order.application.command.event.ProductUnpublished
import com.dbranick.demo.order.domain.*
import com.dbranick.demo.order.domain.event.EventBus
import com.dbranick.demo.order.utils.logger
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
internal class OrderCommandService(
	private val orderRepository: OrderRepository,
	private val domainEventBus: EventBus,
	private val productValidationPolicy: ProductValidationPolicy
) {

	@Transactional
	fun createOrder(command: CreateOrderCommand): String {
		val order = Order.new(
			customer = CustomerId(command.customerId),
			eventBus = domainEventBus
		)

		orderRepository.save(order)
		return order.orderId.value
	}

	@Transactional
	fun addProductToCart(id: String, command: AddProductToOrderCommand) {
		val orderId = OrderId(id)
		val order = orderRepository.findOrder(orderId) ?: throw OrderNotFoundException(orderId)

		order.addProduct(domainEventBus, ProductId(command.productId), Quantity(command.quantity), productValidationPolicy)
		orderRepository.save(order)
	}

	@Transactional
	fun changeProductQuantity(id: String, command: EditProductQuantityCommand) {
		val orderId = OrderId(id)
		val order = orderRepository.findOrder(orderId) ?: throw OrderNotFoundException(orderId)

		order.changeQuantity(domainEventBus, ProductLineId(command.productLineId), Quantity(command.quantity))
		orderRepository.save(order)
	}

	@Transactional
	fun removeProduct(id: String, command: RemoveProductCommand) {
		val orderId = OrderId(id)
		val order = orderRepository.findOrder(orderId) ?: throw OrderNotFoundException(orderId)

		order.removeProductLine(domainEventBus, ProductLineId(command.productLineId))
		orderRepository.save(order)
	}

	@Transactional
	fun selectPaymentMethod(id: String, command: SelectPaymentMethodCommand) {
		val orderId = OrderId(id)
		val order = orderRepository.findOrder(orderId) ?: throw OrderNotFoundException(orderId)

		order.selectPaymentMethod(domainEventBus, PaymentMethodId(command.paymentMethodId))
		orderRepository.save(order)
	}

	@Transactional
	fun markOrderAsConfirmed(id: String) {
		val orderId = OrderId(id)
		val order = orderRepository.findOrder(orderId) ?: throw OrderNotFoundException(orderId)

		order.markAsConfirmed(domainEventBus)
		orderRepository.save(order)
	}

	@Transactional
	fun markOrderAsReady(id: String) {
		val orderId = OrderId(id)
		val order = orderRepository.findOrder(orderId) ?: throw OrderNotFoundException(orderId)

		order.markAsReady(domainEventBus)
		orderRepository.save(order)
	}

	@Transactional
	fun markOrderAsDelivered(id: String) {
		val orderId = OrderId(id)
		val order = orderRepository.findOrder(orderId) ?: throw OrderNotFoundException(orderId)

		order.markAsDelivered(domainEventBus)
		orderRepository.save(order)
	}

	@RabbitListener(queues = ["productsQueue"])
	fun handleProductUnpublishedEvent(event: ProductUnpublished) {
		logger.info("Handled $event")
		generateSequence(0) { it + 50 }
			.map { orderRepository.findNewOrders(it, 50) }
			.takeWhile { !it.isEmpty }
			.flatMap { it.content }
			.forEach { it.removeProduct(domainEventBus, ProductId(event.productId)) }
	}

}

