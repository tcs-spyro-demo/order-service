package com.dbranick.demo.order.application.query

import com.dbranick.demo.order.domain.OrderStatus
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal
import java.time.LocalDateTime
import java.util.*

@Document("order_reports")
internal data class OrderReport(
	@Id
	val id: String,
	val orderNumber: String,
	val status: OrderStatus,
	val lastModificationDate: LocalDateTime,
	val createDate: LocalDateTime,
	val productsLines: List<ProductLineReport>,
	val paymentMethod: PaymentReport?,
	val totalPrice: BigDecimal,
	val customer: CustomerReport
)

internal data class ProductLineReport(
	val productLineId: String,
	val product: ProductReport,
	val quantity: BigDecimal,
	val price: BigDecimal
)

internal data class ProductReport(
	val productId: String,
	val sku: String,
	val price: BigDecimal,
	val name: String
)

internal class PaymentReport(
	val paymentId: String,
	val name: String
)

internal class CustomerReport(
	val customerId: String,
	val firstName: String,
	val lastName: String,
	val email: String
)