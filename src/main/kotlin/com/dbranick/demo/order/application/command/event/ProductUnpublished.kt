package com.dbranick.demo.order.application.command.event

internal data class ProductUnpublished(
	val productId: String
)