package com.dbranick.demo.order.application.query

import com.dbranick.demo.order.domain.OrderStatus
import com.dbranick.demo.order.domain.event.*
import com.dbranick.demo.order.domain.event.OrderCreated
import com.dbranick.demo.order.domain.event.OrderPaid
import com.dbranick.demo.order.domain.event.ProductLineCreated
import com.dbranick.demo.order.domain.event.ProductLineQuantityChanged
import com.dbranick.demo.order.domain.event.ProductLineRemoved
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

@Component
internal class DomainEventListener(
	private val orderReportProjection: OrderReportProjection
) {

	@EventListener(OrderCreated::class)
	fun handleOnOrderCreatedEvent(event: OrderCreated) {
		with(event) {
			orderReportProjection.createOrderReport(orderId, number, customerId, lastModificationDate)
		}
	}

	@EventListener(ProductLineCreated::class)
	fun handleOnProductLineCreated(event: ProductLineCreated) {
		with(event) {
			orderReportProjection.updateOrderReport(orderId, productLineId, quantity, productId, lastModificationDate )
		}
	}

	@EventListener(ProductLineQuantityChanged::class)
	fun handleOnProductQuantityChanged(event: ProductLineQuantityChanged) {
		with(event) {
			orderReportProjection.updateOrderReport(orderId, lastModificationDate, productLineId, quantity)
		}
	}

	@EventListener(ProductLineRemoved::class)
	fun handleOnProductRemoved(event: ProductLineRemoved) {
		with(event) {
			orderReportProjection.updateOrderReport(orderId, lastModificationDate, productLineId)
		}
	}

	@EventListener(OrderPaid::class)
	fun handleOnOrderPaid(event: OrderPaid) {
		with(event) {
			orderReportProjection.updateOrderReport(orderId, lastModificationDate, paymentMethodId)
		}
	}

	@EventListener(OrderConfirmed::class)
	fun handleOnOrderConfirmed(event: OrderConfirmed) {
		with(event) {
			orderReportProjection.updateOrderReport(orderId, lastModificationDate, OrderStatus.CONFIRMED)
		}
	}

	@EventListener(OrderPrepared::class)
	fun handleOnOrderConfirmed(event: OrderPrepared) {
		with(event) {
			orderReportProjection.updateOrderReport(orderId, lastModificationDate, OrderStatus.READY)
		}
	}

	@EventListener(OrderDelivered::class)
	fun handleOnOrderDelivered(event: OrderDelivered) {
		with(event) {
			orderReportProjection.updateOrderReport(orderId, lastModificationDate, OrderStatus.DELIVERED)
		}
	}
}