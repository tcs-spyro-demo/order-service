package com.dbranick.demo.order.application.query

import com.dbranick.demo.order.domain.*
import com.dbranick.demo.order.domain.CustomerId
import com.dbranick.demo.order.domain.LastModificationDate
import com.dbranick.demo.order.domain.OrderId
import com.dbranick.demo.order.domain.OrderNumber
import com.dbranick.demo.order.domain.PaymentMethodId

internal interface OrderReportProjection {

	fun createOrderReport(
		id: OrderId,
		orderNumber: OrderNumber,
		customerId: CustomerId,
		lastModificationDate: LastModificationDate
	)

	fun updateOrderReport(
		id: OrderId,
		lastModificationDate: LastModificationDate,
		status: OrderStatus
	)

	fun updateOrderReport(
		orderId: OrderId,
		productLineId: ProductLineId,
		quantity: Quantity,
		productId: ProductId,
		lastModificationDate: LastModificationDate
	)

	fun updateOrderReport(
		id: OrderId,
		lastModificationDate: LastModificationDate,
		paymentMethodId: PaymentMethodId
	)

	fun updateOrderReport(
		id: OrderId,
		lastModificationDate: LastModificationDate,
		productLineId: ProductLineId,
		quantity: Quantity,
	)

	fun updateOrderReport(
		id: OrderId,
		lastModificationDate: LastModificationDate,
		productLineId: ProductLineId,
	)
}
