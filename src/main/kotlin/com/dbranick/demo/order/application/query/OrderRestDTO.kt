package com.dbranick.demo.order.application.query

import java.math.BigDecimal
import java.time.LocalDateTime

internal class OrderRestDTO(
	val id: String,
	val number: String,
	val customer: CustomerRestDTO,
	val totalPrice: BigDecimal,
	val status: String,
	val lastModificationDate: LocalDateTime,
	val createDate: LocalDateTime,
	val productLines: List<ProductLineRestDTO>,
	val paymentMethod: PaymentRestDTO?,
)

internal class CustomerRestDTO(
	val id: String,
	val firstName: String,
	val lastName: String,
	val email: String
)

internal class ProductLineRestDTO(
	val id: String,
	val product: ProductRestDTO,
	val quantity: BigDecimal,
	val price: BigDecimal
)

internal class ProductRestDTO(
	val id: String,
	val sku: String,
	val price: BigDecimal,
	val name: String
)

internal class PaymentRestDTO(
	val id: String,
	val name: String
)