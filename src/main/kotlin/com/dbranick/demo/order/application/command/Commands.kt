package com.dbranick.demo.order.application.command

import java.math.BigDecimal
import java.util.*

internal interface CreateOrderCommand {
	val customerId: String
}

internal interface AddProductToOrderCommand {
	val productId: String
	val quantity: BigDecimal
}

internal interface EditProductQuantityCommand {
	val productLineId: String
	val quantity: BigDecimal
}

internal interface SelectPaymentMethodCommand {
	val paymentMethodId: String
}

internal interface RemoveProductCommand {
	val productLineId: String
}