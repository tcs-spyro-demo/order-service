package com.dbranick.demo.order.application.query

internal interface OrderQueryService {

	fun getOrder(id: String): OrderRestDTO
}