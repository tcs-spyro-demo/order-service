package com.dbranick.demo.order.infrastructure.query

import com.dbranick.demo.order.application.query.*
import com.dbranick.demo.order.application.query.CustomerRestDTO
import com.dbranick.demo.order.application.query.OrderQueryService
import com.dbranick.demo.order.application.query.OrderReport
import com.dbranick.demo.order.application.query.OrderRestDTO
import com.dbranick.demo.order.application.query.ProductLineRestDTO
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.stereotype.Component
import java.lang.IllegalStateException

@Component
internal class MongoOrderQueryService(
	private val mongoTemplate: MongoTemplate
) : OrderQueryService {

	override fun getOrder(id: String): OrderRestDTO {
		val orderReport = mongoTemplate.findById(id, OrderReport::class.java)
			?: throw IllegalStateException("Order report not found")

		return orderReport.toRestDTO()
	}

	private fun OrderReport.toRestDTO() = OrderRestDTO(
		id = id,
		number = orderNumber,
		customer = CustomerRestDTO(
			id = customer.customerId,
			firstName = customer.firstName,
			lastName = customer.lastName,
			email = customer.email
		),
		totalPrice = totalPrice,
		status = status.name,
		lastModificationDate = lastModificationDate,
		createDate = createDate,
		productLines = productsLines.map { it.toRestDTO() },
		paymentMethod = paymentMethod?.let { PaymentRestDTO(it.paymentId, it.name) }
	)

	private fun ProductLineReport.toRestDTO() = ProductLineRestDTO(
		id = productLineId,
		product = ProductRestDTO(
			id = product.productId,
			sku = product.sku,
			price = product.price,
			name = product.name
		),
		quantity = quantity,
		price = price
	)
}


