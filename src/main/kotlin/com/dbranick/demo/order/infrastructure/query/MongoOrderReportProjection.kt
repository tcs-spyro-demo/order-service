package com.dbranick.demo.order.infrastructure.query

import com.dbranick.demo.order.application.query.*
import com.dbranick.demo.order.domain.*
import com.dbranick.demo.order.infrastructure.external.ExternalCustomerRepository
import com.dbranick.demo.order.infrastructure.external.ExternalProductRepository
import com.dbranick.demo.order.utils.replace
import com.dbranick.demo.order.utils.replaceOrNew
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.stereotype.Component
import java.math.BigDecimal

@Component
internal class MongoOrderReportProjection(
	private val mongoTemplate: MongoTemplate,
	private val externalProductRepository: ExternalProductRepository,
	private val externalCustomerRepository: ExternalCustomerRepository
) : OrderReportProjection {

	override fun createOrderReport(
		id: OrderId,
		orderNumber: OrderNumber,
		customerId: CustomerId,
		lastModificationDate: LastModificationDate
	) {
		val customer = externalCustomerRepository.getCustomerData(customerId)
			?: throw CustomerNotFoundException(customerId)

		val report = OrderReport(
			id = id.value,
			orderNumber = orderNumber.value,
			status = OrderStatus.NEW,
			lastModificationDate = lastModificationDate.value,
			customer = customer,
			createDate = lastModificationDate.value,
			productsLines = emptyList(),
			paymentMethod = null,
			totalPrice = BigDecimal.ZERO
		)

		mongoTemplate.save(report)
	}

	override fun updateOrderReport(
		id: OrderId,
		lastModificationDate: LastModificationDate,
		status: OrderStatus
	) {
		updateOrderReport(id) { report ->
			report.copy(
				lastModificationDate = lastModificationDate.value,
				status = status
			)
		}
	}

	override fun updateOrderReport(id: OrderId, productLineId: ProductLineId, quantity: Quantity, productId: ProductId, lastModificationDate: LastModificationDate) {
		updateOrderReport(id) { report ->
			val productLines = report.productsLines
				.replaceOrNew(createProductLineReport(productLineId, quantity, productId)) {
					it.productLineId == productLineId.value
				}

			report.copy(
				lastModificationDate = lastModificationDate.value,
				productsLines = productLines,
				totalPrice = productLines.sumOf { it.price }
			)
		}
	}

	override fun updateOrderReport(id: OrderId, lastModificationDate: LastModificationDate, paymentMethodId: PaymentMethodId) {
		updateOrderReport(id) { report ->
			report.copy(
				lastModificationDate = lastModificationDate.value,
				paymentMethod = PaymentReport(paymentMethodId.value, "Demo payment method"),
				status = OrderStatus.PAID
			)
		}
	}

	override fun updateOrderReport(id: OrderId, lastModificationDate: LastModificationDate, productLineId: ProductLineId, quantity: Quantity) {
		updateOrderReport(id) { report ->
			val productReport = report.productsLines.find { it.productLineId == productLineId.value }
				?: throw MissingProductLineException(productLineId)

			val updatedProductLineReport = productReport.copy(
				quantity = quantity.value,
				price = quantity.value * productReport.product.price
			)

			val productLines = report.productsLines.replace(updatedProductLineReport) {
				it.productLineId == productLineId.value
			}

			report.copy(
				lastModificationDate = lastModificationDate.value,
				productsLines = productLines,
				totalPrice = productLines.sumOf { it.price }
			)
		}
	}

	override fun updateOrderReport(id: OrderId, lastModificationDate: LastModificationDate, productLineId: ProductLineId) {
		updateOrderReport(id) { report ->
			val productLines = report.productsLines.toMutableList()
			productLines.removeIf { it.productLineId == productLineId.value }

			report.copy(
				productsLines = productLines,
				totalPrice = productLines.sumOf { it.price }
			)
		}
	}

	private fun updateOrderReport(id: OrderId, body: (OrderReport) -> OrderReport) {
		val orderReport = mongoTemplate.findById(id.value, OrderReport::class.java)
			?: throw OrderNotFoundException(id)
		mongoTemplate.save(body(orderReport))
	}

	private fun createProductLineReport(productLineId: ProductLineId, quantity: Quantity, productId: ProductId): ProductLineReport {
		val productReport = getProductReport(productId)
		return ProductLineReport(
			productLineId = productLineId.value,
			product = productReport,
			quantity = quantity.value,
			price = quantity.value * productReport.price
		)
	}

	private fun getProductReport(productId: ProductId): ProductReport {
		return externalProductRepository.findProduct(productId)
			?.let { ProductReport(it.id, it.sku, it.price, it.name) }
			?: throw ProductNotFoundException(productId)
	}
}