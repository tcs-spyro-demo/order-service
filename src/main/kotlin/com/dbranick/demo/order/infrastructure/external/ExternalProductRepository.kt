package com.dbranick.demo.order.infrastructure.external

import com.dbranick.demo.order.domain.ProductData
import com.dbranick.demo.order.domain.ProductId
import com.dbranick.demo.order.domain.ProductRepository
import com.dbranick.demo.order.utils.logger
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClientBuilder
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
internal class ExternalProductRepository(
	@Value("\${demo.product.url:http://localhost:8080}")
	private val serviceUrl: String,
) : ProductRepository {

	private val httpClient = HttpClientBuilder.create().build()
	private val objectMapper = ObjectMapper()
		.registerModule(KotlinModule())
		.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

	override fun findProduct(productId: ProductId): ProductData? {
		val request = HttpGet("$serviceUrl/api/products/${productId.value}")

		return runCatching { httpClient.execute(request) }
			.map { objectMapper.readValue<ProductData>(it.entity.content) }
			.onFailure { logger.error("Error during getting product data") }
			.getOrNull()
	}

}