package com.dbranick.demo.order.infrastructure.external

import com.dbranick.demo.order.application.query.CustomerReport
import com.dbranick.demo.order.domain.CustomerId
import com.dbranick.demo.order.utils.logger
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClientBuilder
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
internal class ExternalCustomerRepository(
	@Value("\${demo.customer.url:http://localhost:8082}")
	private val serviceUrl: String,
) {
	private val httpClient = HttpClientBuilder.create().build()
	private val objectMapper = ObjectMapper()
		.registerModule(KotlinModule())
		.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)


	fun getCustomerData(customerId: CustomerId): CustomerReport? {

		val request = HttpGet("$serviceUrl/api/customers/${customerId.value}")

		return runCatching { httpClient.execute(request) }
			.map { objectMapper.readValue<CustomerDto>(it.entity.content) }
			.onFailure { logger.error("Error during getting customer data") }
			.map { CustomerReport(it.id, it.firstName, it.lastName, it.email) }
			.getOrNull()
	}

	private class CustomerDto(
		val id: String,
		val firstName: String,
		val lastName: String,
		val email: String
	)
}