package com.dbranick.demo.order.infrastructure.command

import com.dbranick.demo.order.domain.Order
import com.dbranick.demo.order.domain.OrderId
import com.dbranick.demo.order.domain.OrderRepository
import com.dbranick.demo.order.domain.OrderStatus
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.find
import org.springframework.data.mongodb.core.findById
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Component
import kotlin.reflect.KProperty

@Component
internal class MongoOrderRepository(
	private val mongoTemplate: MongoTemplate
) : OrderRepository {

	override fun findOrder(orderId: OrderId): Order? {
		return mongoTemplate.findById(orderId)
	}

	override fun findNewOrders(page: Int, size: Int): PageImpl<Order> {
		val pageable = PageRequest.of(page, size)
		val query = Query.query(where(Order::status).`is`(OrderStatus.NEW))
		val count = mongoTemplate.count(query, Order::class.java)
		val results: List<Order> = mongoTemplate.find(query.with(pageable))
		return PageImpl(results, pageable, count)
	}

	override fun save(order: Order) {
		mongoTemplate.save(order)
	}

	private fun where(property: KProperty<*>) = Criteria.where(property.name)
}