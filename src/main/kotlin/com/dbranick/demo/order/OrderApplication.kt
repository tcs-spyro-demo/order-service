package com.dbranick.demo.order

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
public class OrderApplication

public fun main(args: Array<String>) {
	runApplication<OrderApplication>(*args)
}
