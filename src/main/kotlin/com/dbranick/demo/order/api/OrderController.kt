package com.dbranick.demo.order.api

import com.dbranick.demo.order.application.command.OrderCommandService
import com.dbranick.demo.order.application.query.OrderQueryService
import com.dbranick.demo.order.application.query.OrderRestDTO
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
internal class OrderController(
	private val orderCommandService: OrderCommandService,
	private val orderQueryService: OrderQueryService
) {

	@ApiOperation("Create new order")
	@PostMapping("/orders")
	fun createOrder(
		@ApiParam("Command", required = true)
		@RequestBody command: CreateOrderRestCommand
	): ResponseEntity<String> {
		return ResponseEntity(orderCommandService.createOrder(command), HttpStatus.CREATED)
	}

	@ApiOperation("Add product to order")
	@PutMapping("/orders/{id}/add-product")
	fun addProductToOrder(
		@ApiParam("Order ID", required = true)
		@PathVariable id: String,
		@ApiParam("Command", required = true)
		@RequestBody command: AddProductToOrderRestCommand
	) {
		orderCommandService.addProductToCart(id, command)
	}

	@ApiOperation("Change product line quantity")
	@PutMapping("/orders/{id}/change-quantity")
	fun changeProductQuantity(
		@ApiParam("Order ID", required = true)
		@PathVariable id: String,
		@ApiParam("Command", required = true)
		@RequestBody command: EditProductQuantityRestCommand
	) {
		orderCommandService.changeProductQuantity(id, command)
	}

	@ApiOperation("Remove product line")
	@DeleteMapping("/orders/{id}")
	fun removeProduct(
		@ApiParam("Order ID", required = true)
		@PathVariable id: String,
		@ApiParam("Command", required = true)
		@RequestBody command: RemoveProductRestCommand
	) {
		orderCommandService.removeProduct(id, command)
	}

	@ApiOperation("Select payment method")
	@PutMapping("/orders/{id}/payment")
	fun selectPaymentMethod(
		@ApiParam("Order ID", required = true)
		@PathVariable id: String,
		@ApiParam("Command", required = true)
		@RequestBody command: SelectPaymentMethodRestCommand
	) {
		orderCommandService.selectPaymentMethod(id, command)
	}

	@ApiOperation("Mark order as ready")
	@PatchMapping("/orders/{id}/ready")
	fun markAsReady(
		@ApiParam("Order ID", required = true)
		@PathVariable id: String,
	) {
		orderCommandService.markOrderAsReady(id)
	}

	@ApiOperation("Mark order as confirmed")
	@PatchMapping("/orders/{id}/confirmed")
	fun markAsConfirmed(
		@ApiParam("Order ID", required = true)
		@PathVariable id: String,
	) {
		orderCommandService.markOrderAsConfirmed(id)
	}

	@ApiOperation("Mark order as delivered")
	@PatchMapping("/orders/{id}/delivered")
	fun markAsDelivered(
		@ApiParam("Order ID", required = true)
		@PathVariable id: String,
	) {
		orderCommandService.markOrderAsDelivered(id)
	}

	@ApiOperation("Get order")
	@GetMapping("/orders/{id}")
	fun getOrder(
		@ApiParam("Order ID", required = true)
		@PathVariable id: String,
	): OrderRestDTO {
		return orderQueryService.getOrder(id)
	}
}