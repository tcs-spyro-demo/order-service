package com.dbranick.demo.order.api

import com.dbranick.demo.order.application.command.*
import com.dbranick.demo.order.application.command.AddProductToOrderCommand
import com.dbranick.demo.order.application.command.CreateOrderCommand
import com.dbranick.demo.order.application.command.EditProductQuantityCommand
import com.dbranick.demo.order.application.command.RemoveProductCommand
import com.dbranick.demo.order.application.command.SelectPaymentMethodCommand
import java.math.BigDecimal
import java.util.*

internal class CreateOrderRestCommand(
	override val customerId: String,
) : CreateOrderCommand

internal class AddProductToOrderRestCommand(
	override val productId: String,
	override val quantity: BigDecimal,
) : AddProductToOrderCommand

internal class EditProductQuantityRestCommand(
	override val productLineId: String,
	override val quantity: BigDecimal,
) : EditProductQuantityCommand

internal class RemoveProductRestCommand(
	override val productLineId: String
) : RemoveProductCommand

internal class SelectPaymentMethodRestCommand(
	override val paymentMethodId: String
) : SelectPaymentMethodCommand