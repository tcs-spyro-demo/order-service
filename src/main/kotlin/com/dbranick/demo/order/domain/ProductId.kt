package com.dbranick.demo.order.domain

internal data class ProductId(val value: String)
