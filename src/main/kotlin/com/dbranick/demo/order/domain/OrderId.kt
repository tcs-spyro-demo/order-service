package com.dbranick.demo.order.domain

import com.fasterxml.jackson.annotation.JsonValue
import java.util.*

internal data class OrderId(
	@JsonValue val value: String
) {

	constructor() : this(UUID.randomUUID().toString())
}
