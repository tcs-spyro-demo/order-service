package com.dbranick.demo.order.domain

import org.springframework.data.domain.PageImpl

internal interface OrderRepository {

	fun findOrder(orderId: OrderId): Order?
	fun save(order: Order)
	fun findNewOrders(page: Int, size: Int): PageImpl<Order>
}