package com.dbranick.demo.order.domain.event

import com.dbranick.demo.order.domain.*
import com.dbranick.demo.order.domain.LastModificationDate
import com.dbranick.demo.order.domain.OrderId
import com.dbranick.demo.order.domain.ProductId
import com.dbranick.demo.order.domain.ProductLineId
import com.dbranick.demo.order.domain.Quantity
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import java.io.Serializable

internal interface EventBus {
	fun publish(event: DomainEvent)
}

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes(
	JsonSubTypes.Type(value = OrderCreated::class, name = "OrderCreated"),
	JsonSubTypes.Type(value = OrderPaid::class, name = "OrderPaid"),
	JsonSubTypes.Type(value = OrderConfirmed::class, name = "OrderConfirmed"),
	JsonSubTypes.Type(value = OrderPrepared::class, name = "OrderPrepared"),
	JsonSubTypes.Type(value = OrderDelivered::class, name = "OrderDelivered")
)
internal sealed class DomainEvent : Serializable {
	abstract val orderId: OrderId
	abstract val lastModificationDate: LastModificationDate
}

internal class ProductLineCreated(
	override val orderId: OrderId,
	override val lastModificationDate: LastModificationDate,
	val productId: ProductId,
	val productLineId: ProductLineId,
	val quantity: Quantity
) : DomainEvent()

internal class ProductLineQuantityChanged(
	override val orderId: OrderId,
	override val lastModificationDate: LastModificationDate,
	val productLineId: ProductLineId,
	val quantity: Quantity
) : DomainEvent()

internal class ProductLineRemoved(
	override val orderId: OrderId,
	override val lastModificationDate: LastModificationDate,
	val productLineId: ProductLineId,
) : DomainEvent()

internal class OrderCreated(
	override val orderId: OrderId,
	override val lastModificationDate: LastModificationDate,
	val customerId: CustomerId,
	val number: OrderNumber
) : DomainEvent()

internal class OrderPaid(
	override val orderId: OrderId,
	override val lastModificationDate: LastModificationDate,
	val paymentMethodId: PaymentMethodId
) : DomainEvent()

internal class OrderConfirmed(
	override val orderId: OrderId,
	override val lastModificationDate: LastModificationDate,
) : DomainEvent()

internal class OrderPrepared(
	override val orderId: OrderId,
	override val lastModificationDate: LastModificationDate,
) : DomainEvent()

internal class OrderDelivered(
	override val orderId: OrderId,
	override val lastModificationDate: LastModificationDate,
) : DomainEvent()