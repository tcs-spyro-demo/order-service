package com.dbranick.demo.order.domain

import org.springframework.stereotype.Component

internal interface ProductValidationPolicy {
	fun isValid(productId: ProductId): Boolean
}

@Component
internal class ProductValidator(
	private val productRepository: ProductRepository
) : ProductValidationPolicy {
	override fun isValid(productId: ProductId): Boolean {
		return productRepository.findProduct(productId) != null
	}
}