package com.dbranick.demo.order.domain

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.math.BigDecimal

@ResponseStatus(HttpStatus.NOT_FOUND, reason = "Order not found")
internal class OrderNotFoundException(orderId: OrderId) : Exception("Order $orderId not found")

@ResponseStatus(HttpStatus.BAD_REQUEST, reason = "Product modification not allowed for current order status")
internal class ProductModificationNotAvailableOnPresentStatusException(
	currentStatus: OrderStatus,
	requiredStatus: OrderStatus
) : Exception("Product modifications are allowed only for $requiredStatus order status but now is $currentStatus")

@ResponseStatus(HttpStatus.BAD_REQUEST, reason = "Payment method select is only available for NEW order status.")
internal class PaymentMethodSelectOnlyAvailableForNewOrderStatusException(currentStatus: OrderStatus)
	: Exception("Payment method select is only available for NEW order status but current is $currentStatus")

@ResponseStatus(HttpStatus.BAD_REQUEST, reason = "Incorrect order status.")
internal class IncorrectOrderStatusException(
	currentStatus: OrderStatus,
	requiredStatus: OrderStatus
) : Exception("Incorrect order status. Current $currentStatus but is required $requiredStatus")

@ResponseStatus(HttpStatus.BAD_REQUEST, reason = "Product invalid.")
internal class ProductInvalidException(productId: ProductId) : Exception("Product $productId invalid")

@ResponseStatus(HttpStatus.BAD_REQUEST, reason = "Order must contains any product to pay.")
internal class OrderMustContainsAnyProductToPayException(orderId: OrderId)
	: Exception("Order ${orderId.value} must contains any product to pay")

@ResponseStatus(HttpStatus.BAD_REQUEST, reason = "Quantity must be greater or equals zero.")
internal class QuantityMustBeGreaterOrEqualsZero(value: BigDecimal)
	: Exception("Quantity must be greater or equals zero, but is $value")

@ResponseStatus(HttpStatus.NOT_FOUND, reason = "Product line not found.")
internal class MissingProductLineException(productLineId: ProductLineId) :
	Exception("Product line $productLineId is missing")

@ResponseStatus(HttpStatus.NOT_FOUND, reason = "Customer not found")
internal class CustomerNotFoundException(customerId: CustomerId)
	: Exception("Customer ${customerId.value} not found.")

@ResponseStatus(HttpStatus.NOT_FOUND, reason = "Product not found")
internal class ProductNotFoundException(productId: ProductId)
	: Exception("Product ${productId.value} not found")