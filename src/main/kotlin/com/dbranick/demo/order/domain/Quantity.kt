package com.dbranick.demo.order.domain

import com.dbranick.demo.order.utils.validate
import java.math.BigDecimal

internal data class Quantity(
	val value: BigDecimal
) {
	init {
		validate(value >= BigDecimal.ZERO) { QuantityMustBeGreaterOrEqualsZero(value) }
	}

	operator fun plus(other: Quantity) = copy(value = value + other.value)
	operator fun times(other: Int) = copy(value = value * other.toBigDecimal())
	fun isPositive() = value > BigDecimal.ZERO
}