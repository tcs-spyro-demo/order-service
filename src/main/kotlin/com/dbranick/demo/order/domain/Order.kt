package com.dbranick.demo.order.domain

import com.dbranick.demo.order.domain.event.*
import com.dbranick.demo.order.domain.event.EventBus
import com.dbranick.demo.order.domain.event.OrderConfirmed
import com.dbranick.demo.order.domain.event.OrderPaid
import com.dbranick.demo.order.domain.event.ProductLineQuantityChanged
import com.dbranick.demo.order.domain.event.ProductLineRemoved
import com.dbranick.demo.order.utils.validate
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

@Document("orders")
internal class Order private constructor(
	val customer: CustomerId,
) {

	@Id
	var orderId: OrderId = OrderId()
		private set
	var orderNumber: OrderNumber = OrderNumber.new()
		private set
	var productLines: ProductLines = ProductLines()
		private set
	var lastModificationDate: LastModificationDate = LastModificationDate.now()
		private set
	@Indexed(name = "order_status_idx")
	var status: OrderStatus = OrderStatus.NEW
		private set
	var paymentMethod: PaymentMethodId? = null
		private set

	fun addProduct(eventBus: EventBus, product: ProductId, quantity: Quantity, productValidationPolicy: ProductValidationPolicy) {
		validate(status == OrderStatus.NEW) {
			ProductModificationNotAvailableOnPresentStatusException(currentStatus = status, requiredStatus = OrderStatus.NEW)
		}
		validate(productValidationPolicy.isValid(product)) {
			ProductInvalidException(product)
		}

		productLines.modifyByProductId(product) {
			changeProductLineQuantity(eventBus, it.productLineId, it.quantity + quantity)
		} orElseNew {
			createProductLine(eventBus, it, product, quantity)
		}
	}

	fun changeQuantity(eventBus: EventBus, productLineId: ProductLineId, quantity: Quantity) {
		validate(status == OrderStatus.NEW) {
			ProductModificationNotAvailableOnPresentStatusException(currentStatus = status, requiredStatus = OrderStatus.NEW)
		}

		if (quantity.isPositive()) {
			changeProductLineQuantity(eventBus, productLineId, quantity)
		} else {
			removeProductLine(eventBus, productLineId)
		}
	}

	private fun changeProductLineQuantity(eventBus: EventBus, productLineId: ProductLineId, quantity: Quantity) {
		val productLine = productLines[productLineId]
		productLine.quantity = quantity
		lastModificationDate = LastModificationDate.now()
		eventBus.publish(ProductLineQuantityChanged(orderId, lastModificationDate, productLineId, quantity))
	}

	fun removeProduct(eventBus: EventBus, productId: ProductId) {
		productLines.findByProduct(productId)
			?.let { removeProductLine(eventBus, it.productLineId) }
	}

	fun removeProductLine(eventBus: EventBus, productLineId: ProductLineId) {
		validate(status == OrderStatus.NEW) {
			ProductModificationNotAvailableOnPresentStatusException(currentStatus = status, requiredStatus = OrderStatus.NEW)
		}

		productLines.remove(productLineId)?.let {
			lastModificationDate = LastModificationDate.now()
			eventBus.publish(ProductLineRemoved(orderId, lastModificationDate, productLineId))
		}
	}


	fun selectPaymentMethod(eventBus: EventBus, paymentMethodId: PaymentMethodId) {
		validate(status == OrderStatus.NEW) {
			PaymentMethodSelectOnlyAvailableForNewOrderStatusException(status)
		}
		validate(!productLines.isEmpty()) {
			OrderMustContainsAnyProductToPayException(orderId)
		}

		status = OrderStatus.PAID
		paymentMethod = paymentMethodId
		lastModificationDate = LastModificationDate.now()
		eventBus.publish(OrderPaid(orderId, lastModificationDate, paymentMethodId))
	}

	fun markAsConfirmed(eventBus: EventBus) {
		validate(status == OrderStatus.PAID) {
			IncorrectOrderStatusException(currentStatus = status, requiredStatus = OrderStatus.PAID)
		}

		status = OrderStatus.CONFIRMED
		lastModificationDate = LastModificationDate.now()
		eventBus.publish(OrderConfirmed(orderId, lastModificationDate))
	}

	fun markAsReady(eventBus: EventBus) {
		validate(status == OrderStatus.CONFIRMED) {
			IncorrectOrderStatusException(currentStatus = status, requiredStatus = OrderStatus.CONFIRMED)
		}

		status = OrderStatus.READY
		lastModificationDate = LastModificationDate.now()
		eventBus.publish(OrderPrepared(orderId, lastModificationDate))
	}

	fun markAsDelivered(eventBus: EventBus) {
		validate(status == OrderStatus.READY) {
			IncorrectOrderStatusException(currentStatus = status, requiredStatus = OrderStatus.DELIVERED)
		}

		status = OrderStatus.DELIVERED
		lastModificationDate = LastModificationDate.now()
		eventBus.publish(OrderDelivered(orderId, lastModificationDate))
	}

	private fun createProductLine(
		eventBus: EventBus,
		productLineId: ProductLineId,
		product: ProductId,
		quantity: Quantity
	): ProductLine {
		return ProductLine(productLineId, product, quantity)
			.also {
				eventBus.publish(ProductLineCreated(
					orderId,
					lastModificationDate,
					product,
					productLineId,
					quantity
				))
			}
	}

	companion object {
		fun new(eventBus: EventBus, customer: CustomerId) = Order(customer)
			.also {
				eventBus.publish(OrderCreated(
					it.orderId,
					it.lastModificationDate,
					customer,
					it.orderNumber
				))
			}
	}
}