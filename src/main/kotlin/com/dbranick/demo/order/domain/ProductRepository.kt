package com.dbranick.demo.order.domain

import java.math.BigDecimal
import java.util.*

internal interface ProductRepository {

	fun findProduct(productId: ProductId): ProductData?
}


internal class ProductData(
	val id: String,
	val name: String,
	val sku: String,
	val price: BigDecimal,
)