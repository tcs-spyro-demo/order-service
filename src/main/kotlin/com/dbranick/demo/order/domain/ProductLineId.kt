package com.dbranick.demo.order.domain

import java.util.*

internal data class ProductLineId(val value: String) {
	constructor() : this(UUID.randomUUID().toString())
}