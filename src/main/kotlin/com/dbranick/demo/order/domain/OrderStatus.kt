package com.dbranick.demo.order.domain

internal enum class OrderStatus {
	NEW,

	PAID,

	CONFIRMED,

	READY,

	DELIVERED
}