package com.dbranick.demo.order.domain

import com.fasterxml.jackson.annotation.JsonValue
import java.sql.Timestamp
import java.time.LocalDateTime
import java.time.ZoneOffset

internal class OrderNumber private constructor(@JsonValue val value: String) {
	companion object {
		fun new(): OrderNumber {
			val timestamp = Timestamp.valueOf(LocalDateTime.now(ZoneOffset.UTC))
			return OrderNumber(timestamp.time.toString())
		}
	}
}
