package com.dbranick.demo.order.domain

internal class ProductLine(
	val productLineId: ProductLineId,
	val product: ProductId,
	var quantity: Quantity,
)