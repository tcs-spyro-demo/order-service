package com.dbranick.demo.order.domain


internal class ProductLines(
	private val map: MutableMap<String, ProductLine> = mutableMapOf()
) : MutableMap<String, ProductLine> by map {

	operator fun get(productLineId: ProductLineId): ProductLine =
		map[productLineId.value] ?: throw MissingProductLineException(productLineId)

	fun remove(productLineId: ProductLineId): ProductLine? {
		val oldValue = map[productLineId.value]
		map.remove(productLineId.value)
		return oldValue
	}

	fun modifyByProductId(productId: ProductId, modification: (ProductLine) -> Unit): OrElseNew {
		val productLine = findByProduct(productId)
		return if (productLine != null) {
			modification(productLine)
			object : OrElseNew {
				override fun orElseNew(body: (ProductLineId) -> ProductLine): ProductLine = productLine
			}
		} else {
			object : OrElseNew {
				override fun orElseNew(body: (ProductLineId) -> ProductLine): ProductLine {
					return get(new(body))
				}
			}
		}
	}

	fun findByProduct(productId: ProductId): ProductLine? =
		map.values.find { it.product == productId }

	fun new(body: (ProductLineId) -> ProductLine): ProductLineId {
		val id = ProductLineId()
		map[id.value] = body(id)
		return id
	}

}

internal interface OrElseNew {
	infix fun orElseNew(body: (ProductLineId) -> ProductLine): ProductLine
}
