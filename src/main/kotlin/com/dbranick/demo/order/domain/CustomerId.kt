package com.dbranick.demo.order.domain

import com.fasterxml.jackson.annotation.JsonValue

internal data class CustomerId(@JsonValue val value: String)
