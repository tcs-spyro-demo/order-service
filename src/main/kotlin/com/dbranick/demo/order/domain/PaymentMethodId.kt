package com.dbranick.demo.order.domain

import com.fasterxml.jackson.annotation.JsonValue

internal data class PaymentMethodId(@JsonValue val value: String)
