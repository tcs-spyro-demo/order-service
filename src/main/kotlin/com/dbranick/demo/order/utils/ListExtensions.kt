package com.dbranick.demo.order.utils

public fun <T> List<T>.replace(newValue: T, block: (T) -> Boolean): List<T> {
	return map {
		if (block(it)) newValue else it
	}
}

public fun <T> List<T>.replaceOrNew(newValue: T, block: (T) -> Boolean): List<T> {
	return if (none { block(it) }) {
		this.toMutableList().apply {
			add(newValue)
		}
	} else {
		replace(newValue) { block(it) }
	}
}